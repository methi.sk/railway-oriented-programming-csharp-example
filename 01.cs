public class AccountController {


    // The main process
    public Result UpdateEmail([FormBody] string reqBody) {

        dynamic data = JsonConvert.DeserializeObject(reqBody);
        string accountId = data.accountId;
        string newEmail = data.newEmail;
        Account account = AccountRepository.GetAccountById(accountId);
        account.setEmail(newEmail);
        AccountRepository.Update(account);
        
        return new Result(){StatusCode = 200};
    }

}
