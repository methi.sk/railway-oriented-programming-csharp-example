public class AccountController {


    // Create small functions to do some low-level works, 
    // all exceptions must be thrown out from those functions,
    // and those exceptions should be caught only in high-level code
    public Result UpdateEmail([FormBody] string reqBody) {
        try {
            UpdateEmailRequest data = this.extractUpdateEmailRequestBody(reqBody);
            Account account = AccountRepository.getAccountById(data.accountId);
            account.SetEmail(newEmail);
            AccountRepository.update(account);
        }
        catch(Exception e) {
            if(e is JsonSerializationException) {
                return new Result(){StatusCode = 500};
            }
            if(e is DatabaseException) {
                return new Result(){StatusCode = 500};
            }
            if(e is NotFoundException) {
                return new Result(){StatusCode = 404};
            }
            if(e is BadRequestException) {
                return new Result(){StatusCode = 400};
            }
            return new Result(){StatusCode = 500};
        }

        return new Result(){StatusCode = 200};
    }

}
