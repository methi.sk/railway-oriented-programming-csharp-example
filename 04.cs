public class AccountController {


    // Optionally, try to use only single catch statement
    public Result UpdateEmail([FormBody] string reqBody) {
        try {
            dynamic data = JsonConvert.DeserializeObject(reqBody);
            string accountId = data.accountId;
            string newEmail = data.newEmail;
            if(string.IsNullOrEmpty(accountId) || string.IsNullOrEmpty(newEmail)) {
                throw new BadRequestException()
            }
            Account account = AccountRepository.GetAccountById(accountId);
            if(account == null) {
                throw new NotFoundException()
            }
            if(!Util.validateEmail(newEmail)) {
                throw new BadRequestException()
            } 
            account.setEmail(newEmail);
            AccountRepository.Update(account);
        }
        catch(Exception e) {
            if(e is JsonSerializationException) {
                return new Result(){StatusCode = 500};
            }
            if(e is DatabaseException) {
                return new Result(){StatusCode = 500};
            }
            if(e is NotFoundException) {
                return new Result(){StatusCode = 404};
            }
            if(e is BadRequestException) {
                return new Result(){StatusCode = 400};
            } 
            return new Result(){StatusCode = 500};
        }

        return new Result(){StatusCode = 200};
    }

}
