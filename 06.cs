public class AccountController {


    // Because the exception handlers should be in top-level code, 
    // in this case it can be in the middleware—the topmost-level of the application
    public Result UpdateEmail([FormBody] string reqBody) {
        
        UpdateEmailRequest data = this.extractUpdateEmailRequestBody(reqBody);
        Account account = AccountRepository.getAccountById(data.accountId);
        account.SetEmail(newEmail);
        AccountRepository.update(account);

        return new Result(){StatusCode = 200};
    }

}
