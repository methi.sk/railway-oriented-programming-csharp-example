public class AccountController {


    // The failure handlings make code hard to read
    public Result UpdateEmail([FormBody] string reqBody) {

        dynamic data;
        string accountId, newEmail;
        Account account;

        try {
            data = JsonConvert.DeserializeObject(reqBody);
            accountId = data.accountId;
            newEmail = data.newEmail;
        }
        catch(JsonSerializationException e) {
            return new Result(){StatusCode = 500};
        }
        if(string.IsNullOrEmpty(accountId) || string.IsNullOrEmpty(newEmail)) {
            return new Result(){StatusCode = 400};
        }
        
        try {
            account = AccountRepository.GetAccountById(accountId);
        }
        catch(DatabaseException e) {
            return new Result(){StatusCode = 500};
        }
        if(account == null) {
            return new Result(){StatusCode = 404};
        }
        if(!Util.validateEmail(newEmail)) {
            return new Result(){StatusCode = 400};
        } 
        try {
            account.setEmail(newEmail);
            AccountRepository.Update(account);
        }
        catch(DatabaseException e) {
            return new Result(){StatusCode = 500};
        }
        return new Result(){StatusCode = 200};
    }
    
}
