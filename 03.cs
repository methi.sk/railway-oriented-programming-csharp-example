public class AccountController {


    // We need to focus only the main process and try to throw out all the failures
    public Result UpdateEmail([FormBody] string reqBody) {
        try {
            dynamic data = JsonConvert.DeserializeObject(reqBody);
            string accountId = data.accountId;
            string newEmail = data.newEmail;
            if(string.IsNullOrEmpty(accountId) || string.IsNullOrEmpty(newEmail)) {
                throw new BadRequestException()
            }
            Account account = AccountRepository.GetAccountById(accountId);
            if(account == null) {
                throw new NotFoundException()
            }
            if(!Util.validateEmail(newEmail)) {
                throw new BadRequestException()
            } 
            account.setEmail(newEmail);
            AccountRepository.Update(account);
        }
        catch(JsonSerializationException e) {
            return new Result(){StatusCode = 500};
        }
        catch(DatabaseException e) {
            return new Result(){StatusCode = 500};
        }
        catch(NotFoundException e) {
            return new Result(){StatusCode = 404};
        }
        catch(BadRequestException e) {
            return new Result(){StatusCode = 400};
        }
        catch(Exception e) {
            return new Result(){StatusCode = 500};
        }

        return new Result(){StatusCode = 200};
        
    }

}
